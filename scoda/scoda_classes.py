from __future__ import division
from __future__ import print_function

import os
import re
import sys
from builtins import object
from builtins import range
from collections import OrderedDict

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import progressbar as pb
import seaborn as sns
import simpy
import yaml
import yamlordereddictloader
from past.builtins import basestring
from past.utils import old_div

from hmc_marine.sequence_tool_utils import SequenceToolEnvironment
from hmc_utils.misc import (get_logger, read_value_from_dict_if_valid, print_banner)

sns.set(rc={"figure.figsize": (16, 10)})

STATE_RUNNING = 1
STATE_WAITING = -1
STATE_IDLE = 0

STATE_DELTA = 0.2

STATE_FIELD = "state"
MASK_FIELD = "mask"
SWITCH_FIELD = "switch"

SCOPE_FIELDS_TO_DUMP_FOR_RESTART = list(["name", "value", "window", "units", "thresholds", "gap"])

# this dictionary is going to hold all the scenarios that have been created in sequential order,
# which can be used for plotting later
SCENARIO_FAMILY = OrderedDict()

PROGRESS_WIDGETS = [pb.Percentage(), ' ', pb.Bar(marker='.', left='[', right=']'), ""]
# widgets = [pb.Percentage(), ' ', pb.Bar(marker=pb.RotatingMarker()),"{:40}".format(message)]
MESSAGE_FORMAT = "{:40}"


def progress_bar_message(counter, maximum, date_time, total_time=0):
    return " {:4d} / {}: {}".format(counter, maximum, date_time, total_time)


class ScopeTimer(object):
    """
    Class to hold the current time belong to a process

    Parameters
    ----------
    time_left_initial: float
        time at the start of the scope
    env_now: float
        current time of the environment
    name: str
        name of this time
    """

    def __init__(self, time_left_initial, env_now=0, name=None):

        self.logger = get_logger(__name__)
        self.name = name
        # time_left keeps track of how much time of the nett scope time is still left
        self.time_left_initial = time_left_initial
        self.time_left = time_left_initial
        self.current_time = env_now
        self.previous_time = None
        self.delta_time = None

        self.start_time_list = list()
        self.stop_time_list = list()
        self.waiting_time_list = list()

        self.is_processing = True

    def reset_timer(self, time_left_initial, env_now=0, is_processing=True):
        """
        Set the time back to zero

        Parameters
        ----------
        time_left_initial: float
            Initial time
        env_now: float
            Current time of this environment
        is_processing: bool
            Flag to indicate if we are processing this scope
        """
        self.time_left = time_left_initial
        self.current_time = env_now
        self.is_processing = is_processing

    def set_interrupt(self, env_now=0):
        """
        Create an interupt  to stop this scope

        Parameters
        ----------
        env_now: int
            current time of the scop
        """
        self.is_processing = False
        if self.current_time not in self.stop_time_list:
            self.stop_time_list.append(env_now)

    def set_processing(self, env_now=0):
        """
        Set the curren scope to processing

        Parameters
        ----------
        env_now: float
            Current time of the environment

        """
        self.is_processing = True
        if self.current_time not in self.start_time_list:
            self.start_time_list.append(env_now)

    def update_time(self, env_now=0):
        """
        Update the current time


        Parameters
        ----------
        env_now: float
            Current time of the environment

        Notes
        -----

        * The routine should be called after a yield timeout of the scope
        """

        # store the previous time based on the latest current time
        self.previous_time = self.current_time

        # update the current time to the env.now time
        self.current_time = env_now

        # get the time step size with respect to the last current time
        self.delta_time = self.current_time - self.previous_time

        # the idea is that we are always switch from working to not working and visa versa when this
        # routine is called
        if self.is_processing:
            # we were working, so the last delta_time we have made progress: subtract the delta from
            # the time_left
            self.time_left -= self.delta_time

            self.logger.debug(
                "Update time with modifying time left {} and switch process"
                "".format(self.time_left, self.is_processing))

            # no we enter a break (due to for instance a storm). Add this time to the stop lift and
            # set the is_progressing flag to false
            self.is_processing = False
            if self.current_time not in self.stop_time_list:
                self.stop_time_list.append(self.current_time)
        else:
            # we were not progressing when this update_routine was called. The last delta_time did
            # not change the time_left, but we start processing now. Switch the flag is_progressing
            # and start the current time as strt time
            self.logger.debug(
                "Update time WITHOUT modifying time left {} and switch process".format(
                    self.time_left,
                    self.is_processing))
            self.is_processing = True
            if self.current_time not in self.start_time_list:
                self.start_time_list.append(self.current_time)
            # in case we have had a stop time stored already (at the beginning of the waiting
            # period), we can store the waiting time as well
            if len(self.stop_time_list) > 0:
                self.waiting_time_list.append(self.current_time - self.stop_time_list[-1])

    def report(self, label=None, start_date_time=None):
        """
        Create a report of the current timer

        Parameters
        ----------
        label: str
            Label to give to the report
        start_date_time: :obj:`DateTime`
            Object holding the date time

        """
        msg = "{:20s} : {}"
        msg2 = "{:20s} : {} h ({})"
        if start_date_time is not None:
            current_date_time = start_date_time + pd.Timedelta(self.current_time, unit="h")
            previous_date_time = start_date_time + pd.Timedelta(self.previous_time, unit="h")
        else:
            current_date_time = ""
            previous_date_time = ""
        if label is not None:
            print(msg.format(" Label", label))
        print(msg.format(" name", self.name))
        print(msg2.format(" current time", self.current_time, current_date_time))
        print(msg2.format(" previous time", self.previous_time, previous_date_time))
        print(msg.format(" previous time", self.previous_time))
        print(msg.format(" delta time", self.delta_time))
        print(msg.format(" time left init", self.time_left_initial))
        print(msg.format(" time left", self.time_left))
        print(msg.format(" start time list", self.start_time_list))
        print(msg.format(" stop time list", self.stop_time_list))
        print(msg.format(" waiting time list", self.waiting_time_list))
        print(msg.format(" is processing ", self.is_processing))


class Consumable(object):
    """
    Class to hold the consumables

    Under development. Does not work yet
    """

    def __init__(self, env, name=None, description=None, consumption_rate=None, replenish_rate=None,
                 stock=0):
        self.logger = get_logger(name)
        self.name = name
        self.description = description
        self.consumption_rate = consumption_rate
        self.replenish_rate = replenish_rate
        self.stock = stock

        self.container = simpy.Container(env, init=stock)

    def window_in_hours(self, window_in_consumables, value_of_consumable):
        """
        Helper method to convert the window in consumable to a window in hours

        Parameters
        ----------
        window_in_consumables: float
           Time window of consumables
        value_of_consumable:  float
           Current amount of consumables

        Returns
        -------
        float:
            Amount of time left  for consumable
        """
        if value_of_consumable < 0:
            window_in_hours = old_div(window_in_consumables, self.replenish_rate)
        elif value_of_consumable > 0:
            window_in_hours = old_div(window_in_consumables, self.consumption_rate)
        else:
            window_in_hours = 0

        return window_in_hours


class ConsumableCollection(object):
    """
    Collection of consumables

    Still under development
    """

    def __init__(self, env, consumable_dictionary):
        self.items = OrderedDict()

        for consumable_name, consumable_properties in consumable_dictionary.items():
            description = read_value_from_dict_if_valid(consumable_properties, "description")
            consumption_rate = read_value_from_dict_if_valid(consumable_properties,
                                                             "consumption_rate", 0)
            replenish_rate = read_value_from_dict_if_valid(consumable_properties, "replenish_rate",
                                                           0)
            stock = read_value_from_dict_if_valid(consumable_properties, "stock", 0)

            self.items[consumable_name] = Consumable(env,
                                                     name=consumable_name,
                                                     description=description,
                                                     consumption_rate=consumption_rate,
                                                     replenish_rate=replenish_rate,
                                                     stock=stock
                                                     )


class Scope(object):
    """
    Class to hold one scope of the project

    Parameters
    ----------
    name: str
        Name of this scope
    value: float
        Duration in hours of this scope of the amount of consumables
    description: str
        Description what this scope does
    window: float
        Minimal required length of the weather window in hour before this scope get executate
    gap: float
        The scope will stop executing 'gap' hours before the end of the weather window
    delay: float
        After we enter a weather window we need to wait 'delay' hours. Not yet implemented
    thresholds: dict
        Contains keys corresponding to the environmental fields with a maximum value.

        Example::

           thresholds:
                    Hs: 3
                    Vw: 10

        Which defines a maximum value for the Hs of 3 (m) and for Vw (wind) of 10 (m/s)
    units: str
        Units belonging to the value. Not used
    date_time_index: :obj:`DateTime`
        Starting date/time of this scope
    has_changed: bool
        Flag indicating if one of the thresholds values has changed. In that case, all the
        threshold will be recalculated
    scenarios: list
        A handle to all scenarios just for reference
    environmental_data_base: obj
        Reference to the environmental data
    parallel_scenario_names: list
        Names of all scenarios which have to run parallel to this scope
    conditional_scenario_names: list
        Names of all the conditional scenerios
    consumable_name: str
        Name of the consumable
    consumable_collection:
        Collection of consumable
    scenario_start_date_time: :obj:`datetime`
        Start date/time of the scenario
    file_base_windowed_data: str
        Name of the data file cotaining the windowed data
    reset_windowed_data: bool
        Reset windowed data

    Notes
    -----

    * The Scope class is the most elementary class holding the properties of one single 'scope' in
      a scenario.

    * Each Scenario can have multiple scope. The only demand is that each scope has a unique name
      from the other scopes in the same scenario

    * The scope will create a field call 'df_data',  which is a pandas dataframe holding the
      threshold data of this scope, ie. it set a list True/False for all the data/time where we can
      work or not. This is stored in the field mask (given by the global variable MASK). Derived
      from the MASK field, the SWITCH field is the derivative of mask, so it is +1 when we move from
      a weather window to a storm and -1 when we move from a storm to a weather window.

    * Finally, the STATE field of df_data is used to keep track of the current state of this scope,
      which can be either STATE_RUNNING (1), STATE_IDLE (0) or STATE_WAITING (-1).

    * Each scope also holds its start_date_time and end_date_time + a list of all intermediate start
      and ends

    * Finally, the 'parallel_scenarios' field holds a dictionary which can contain parallel
      scenarios. In this way, each scenario which list of scope can refer to a new parallel scenario,
      which has a list of scope with new scenarios. etc.

    """

    def __init__(self, name, value,
                 description=None,
                 window=0,
                 gap=0,
                 delay=0,
                 thresholds=dict(),
                 units=None,
                 date_time_index=None,
                 has_changed=False,
                 scenarios=dict(),
                 environmental_data_base=None,
                 parallel_scenario_names=list(),
                 conditional_scenario_names=list(),
                 consumable_name=None,
                 consumable_collection=None,
                 scenario_start_date_time=None,
                 file_base_windowed_data=None,
                 reset_windowed_data=None
                 ):
        self.logger = get_logger(__name__)
        self.name = name
        self.description = description
        self.value = value
        self.window = window
        self.gap = gap
        self.delay = delay
        self.units = units
        self.thresholds = thresholds
        self.consumable = None
        self.consumable_name = consumable_name
        self.consumable_collection = consumable_collection

        # a flag to store to the scope to indicate it has changed
        self.logger.debug(
            "setting scope has_changed flag of {} to {}".format(self.name, has_changed))
        self.has_changed = has_changed
        self.msg_pack_filename = None
        self.info_filename = None

        self.scenarios = scenarios

        self.environmental_data_base = environmental_data_base

        self.finished_scope = False

        self.file_base_windowed_data = file_base_windowed_data
        self.reset_windowed_data = reset_windowed_data

        # make some empty entries for properties to store
        self.timer = ScopeTimer(value, name="timer {}".format(self.name))

        self.scenario_start_date_time = scenario_start_date_time

        self.finished_scope = False
        self.start_of_scope = None  # start of scope in hours wro scenario_start_date_time
        self.end_of_scope = None
        self.scope_process = None  # env.process(self.process_scope(env))

        self.start_date_time = None  # start of scope as a date
        self.end_date_time = None
        self.total_time = None
        self.total_time_in_hours = None

        # handles to store all the processes which allow us to store
        self.working_process = None
        self.weather_check_process = None
        self.wait_processes = dict()

        # create a data from with the date/time as the environment which will contain all weather
        # window length per work scope
        self.df_data = pd.DataFrame(index=date_time_index)

        self.storm_lengths = list()
        self.window_length = list()

        self.parallel_scenarios = OrderedDict()
        self.conditional_scenarios = OrderedDict()
        self.sequential_scenarios = OrderedDict()
        if units in list(self.scenarios.keys()):
            self.logger.debug("We have found a scenario alias {}".format(units))
            sequential_scenario = Scenario(scenarios=self.scenarios,
                                           main_scenario_name=units,
                                           parent_name=self.name,
                                           environmental_data_base=self.environmental_data_base,
                                           consumable_collection=self.consumable_collection,
                                           file_base_windowed_data=self.file_base_windowed_data,
                                           reset_windowed_data=self.reset_windowed_data
                                           )
            self.sequential_scenarios[units] = sequential_scenario

        # the condition and parallel scenarios are now added to the scope
        for con_name in conditional_scenario_names:
            # creating a parallel scenarios
            self.logger.info("Creating a conditional scenario environment for {} : {}"
                             "".format(self.name, con_name))
            conditional_scenario = Scenario(scenarios=self.scenarios,
                                            main_scenario_name=con_name,
                                            parent_name=self.name,
                                            environmental_data_base=self.environmental_data_base,
                                            consumable_collection=self.consumable_collection,
                                            file_base_windowed_data=self.file_base_windowed_data,
                                            reset_windowed_data=self.reset_windowed_data
                                            )

            self.conditional_scenarios[con_name] = conditional_scenario

        # read the parallel scenarios for this scope
        for par_name in parallel_scenario_names:
            # creating a parallel scenarios
            self.logger.info("Creating a parallel scenario environment for {} : {}"
                             "".format(self.name, par_name))
            parallel_scenario = Scenario(scenarios=self.scenarios,
                                         main_scenario_name=par_name,
                                         parent_name=self.name,
                                         environmental_data_base=self.environmental_data_base,
                                         consumable_collection=self.consumable_collection,
                                         file_base_windowed_data=self.file_base_windowed_data,
                                         reset_windowed_data=self.reset_windowed_data
                                         )

            # some plot settings

            self.parallel_scenarios[par_name] = parallel_scenario

    def store_start_of_scope_time(self, current_time):
        """
        store the start of the scope
        """

        self.scope_print("Store the start of the scope")
        self.start_of_scope = current_time
        self.start_date_time = self.scenario_start_date_time + pd.Timedelta(self.start_of_scope,
                                                                            "h")

    def store_end_of_scope_time(self):
        """
        store the end of the scope 
        """
        self.scope_print("Store the end of the scope")
        self.end_of_scope = self.timer.current_time
        self.end_date_time = self.scenario_start_date_time + pd.Timedelta(self.end_of_scope, "h")
        self.total_time = self.end_date_time - self.start_date_time
        self.total_time_in_hours = old_div(self.total_time, pd.Timedelta(1, "h"))

    def check_status(self, date_time):
        """
        Return the status of the current environment

        Returns
        -------
        (bool, bool, bool):
            Three flags
            1. is_storm: indicates if we are in a storm
            2. out_of_data_range: indicates if we are out of range  and the
            3. is_storm_ref_date: is a reference date for the storm
        """

        try:
            is_storm = self.df_data.ix[date_time, MASK_FIELD]
            is_storm_ref_date = date_time
        except KeyError:
            # if the above fails, try to get the states with interpolating
            i_loc = self.df_data[MASK_FIELD].index.get_loc(date_time, method="pad")
            is_storm = self.df_data[MASK_FIELD].iloc[i_loc]
            is_storm_ref_date = self.df_data[MASK_FIELD].index[i_loc]

        out_of_data_range = False
        if date_time is not None and self.end_date_time is not None:
            # in case the end_date_time has been set and we are exceeding the end: set storm to
            # False to prevent the interrupt
            if date_time > self.end_date_time:
                self.logger.debug(
                    "We are out of date range at {} {}".format(date_time, self.end_date_time))
                out_of_data_range = True
        return is_storm, out_of_data_range, is_storm_ref_date

    def full_date(self, hours_since_start):
        """
        Turn the numbers of hours since the start of the scenario into a date/time

        Returns
        -------
        DateTime
            Date/time string of the time at hours_since_stat
        """

        return self.scenario_start_date_time + pd.Timedelta(hours_since_start, "h")

    def process_scope(self, env):
        """
        Process the current scope
        Parameters
        ----------
        env: obj
            Simpy environment

        """

        self.scope_print("start at {}".format(env.now, self.full_date(env.now)))

        current_date_time = self.scenario_start_date_time + pd.Timedelta(env.now, unit="h")

        self.store_start_of_scope_time(env.now)

        is_storm, out_of_range, storm_ref_date = self.check_status(current_date_time)

        if is_storm:
            # we do not want to update the timer as that will update the delta time. Just set the
            # processing flag at false
            self.timer.set_interrupt(env.now)
            self.timer.report(label="{} : Initial stop at start scope".format(self.name),
                              start_date_time=self.scenario_start_date_time)

            # if the state field of the current date time is in a storm, start with waiting for the
            # first weather window
            next_start_window = self.time_to_next_weather_window(env.now)
            self.logger.debug("Start with waiting for the weather at {} for {} h"
                              "".format(current_date_time, next_start_window))
            yield env.timeout(next_start_window)

            self.timer.update_time(env_now=env.now)
            self.timer.report(label="{} : After first wait".format(self.name),
                              start_date_time=self.scenario_start_date_time)

        while not self.finished_scope:

            current_date_time = self.scenario_start_date_time + pd.Timedelta(env.now, unit="h")

            # get the time we have to wait for the next storm + the length of this storm
            next_storm_arrival = self.time_to_next_storm(env.now)

            # the next storm starts at 'next_storm_arrival' hours from now. Get the next window with
            # respect of the start of the next storm, which is the storm length by definition. Add
            # delta_time h after the storm  to make sure we start looking for a window -after- the
            # start of the storm. Since we have changed the reference search time, we can add the
            # same delta_time to the storm_length to make sure we get the right storm_length with
            # respect to the start of the storm without the delta_time
            delta_time = 1
            storm_length = self.time_to_next_weather_window(
                env.now + next_storm_arrival + delta_time) + delta_time

            # the start of the next window (occurring after the first storm arrival) with respect to
            # 'now'
            next_start_window = next_storm_arrival + storm_length

            self.logger.debug("@ {}: Next storm {} h ({}), next window {} h ({}), storm length {} h"
                              "".format(current_date_time,
                                        next_storm_arrival,
                                        current_date_time + pd.Timedelta(next_storm_arrival, "h"),
                                        next_start_window,
                                        current_date_time + pd.Timedelta(next_start_window, "h"),
                                        storm_length))

            # if self.window is not None:
            #    # the window give us the minimum time we can work. If time_left is smaller than window, take window
            #    min_time_needed = max(self.timer.time_left, self.window)
            min_time_needed = self.timer.time_left

            if not self.df_data[MASK_FIELD].any():
                # there are no storm. Make sure we never stop
                next_storm_arrival = 1000000
                next_start_window = next_storm_arrival + 1

            if (min_time_needed > next_storm_arrival) and (min_time_needed <= next_start_window):
                # in the special case that the time left is after the storm arrival time, but before
                # the end of the storm window, we want to increase the time requested. Otherwise in
                # the weather check routine we stop after the first timeout to the next storm as the
                # working time out is scheduled before the end of the storm, which is not possible
                # because we can not work in the storm so always should wait until the end. This
                # check is only necessary for the last update
                self.scope_print("Overruling the time length {} with {}"
                                 "".format(self.timer.time_left, next_start_window))
                request_time = next_start_window
            else:
                # just request the normal amount of time still left
                request_time = min_time_needed
            self.scope_print(
                "calling the work and weather man at {} with {} (ends {})".format(env.now,
                                                                                  request_time,
                                                                                  env.now + request_time))

            # call the processes
            if request_time <= next_storm_arrival:
                # we can finish before the next storm, so only call the working_process
                self.logger.debug(
                    "We can finish before the storm. Skip the weather check process req: {} peek {}"
                    "".format(request_time, env.peek()))
                self.working_process = env.process(self.working_on_scope(env, request_time))
                yield self.working_process
            else:
                # only check for the weather if we can not finish before the start of the storm
                self.logger.debug("Before call next_storm_arrival: {} length {} peek {}".format(
                    next_storm_arrival,
                    storm_length,
                    env.peek()))
                self.weather_check_process = env.process(
                    self.check_weather(env, next_storm_arrival, storm_length))
                self.working_process = env.process(self.working_on_scope(env, request_time))
                yield self.working_process | self.weather_check_process

            if not self.working_process.triggered:
                self.scope_print("We have stopped and started at {}. Time left: {}"
                                 "".format(env.now, self.timer.time_left))
                self.working_process.interrupt("Stopped and Started again at {} (still to go: {}"
                                               "".format(env.now, self.timer.time_left))
            elif self.timer.time_left <= 0:
                self.finished_scope = True
                self.store_end_of_scope_time()
                self.scope_print("Finished this scope @ {} with time left {} : {}"
                                 "".format(env.now, self.timer.time_left, self.finished_scope))
                for process_name, process in self.wait_processes.items():
                    self.scope_print(
                        " also stopping process {} with peek {}".format(process_name, env.peek()))
                    if process.is_alive:
                        process.interrupt(
                            "Calling interrupt for {} at {}".format(process_name, env.now))
                if self.weather_check_process is not None:
                    if self.weather_check_process.is_alive:
                        self.weather_check_process.interrupt(
                            "STOPPPPP in the name of simpy @ {}".format(env.now))

            else:
                # this branch happens if we have overruled the requested time from time_left
                self.timer.set_processing(env.now)
                self.timer.report(
                    label="{} : Edgie case we we stopped storm and working at the same time"
                          "".format(self.name),
                    start_date_time=self.scenario_start_date_time)

        self.scope_print(" After inter now {}".format(env.now))

        self.set_state_for_scope()

    def working_on_scope(self, env, time_left):

        try:
            # here we schedule to work for the time still left
            self.scope_print(
                "START the scope with time left {} at {} with peek {}".format(time_left, env.now,
                                                                              env.peek()))
            yield env.timeout(time_left)

            self.timer.update_time(env_now=env.now)
            self.timer.report(label="{} : After Working timeout".format(self.name),
                              start_date_time=self.scenario_start_date_time)

            # here we have passed the time_left time_out without being interrupted, which means the
            # work is done
            if self.timer.time_left <= 0:
                self.finished_scope = True
                # store the end of this scope
                self.end_of_scope = env.now

                self.scope_print(
                    "Work of scope done at {} (finished {})".format(self.end_of_scope,
                                                                    self.finished_scope))
            else:
                self.scope_print("Still need to continue with time left".format(self.end_of_scope,
                                                                                self.finished_scope))

        except simpy.Interrupt as interrupt:
            # the 'time_left' time_out was stopped by a interrupt, which means a storm has started.
            self.scope_print(
                "Resume after interrupt :{} (finished {} now {})".format(interrupt,
                                                                         self.finished_scope,
                                                                         env.now))

    def wait_for_timeout(self, env, time_out):
        try:
            yield env.timeout(time_out)
        except simpy.Interrupt as interrupt:
            self.logger.debug("STOP waiting for {}".format(interrupt))

    def check_weather(self, env, next_storm_arrival, storm_length):
        try:
            self.scope_print("Next storm at {} over {} h of duration {} h: total : {}"
                             "".format(env.now, next_storm_arrival, storm_length,
                                       storm_length + next_storm_arrival))

            # here we schedule to wait for the next storm
            self.wait_processes["window"] = env.process(
                self.wait_for_timeout(env, next_storm_arrival))
            yield self.wait_processes["window"]

            self.timer.is_processing = True
            self.timer.update_time(env_now=env.now)
            self.timer.report(label="{} : Working upto next storm".format(self.name),
                              start_date_time=self.scenario_start_date_time)

            # now we still need to wait for the storm itself to go over
            self.scope_print(
                "Storm length at {} ({}) over {} h".format(env.now, self.full_date(env.now),
                                                           storm_length))
            self.wait_processes["storm"] = env.process(self.wait_for_timeout(env, storm_length))
            yield self.wait_processes["storm"]

            self.timer.is_processing = False
            self.timer.update_time(env_now=env.now)
            self.timer.report(label="{} : the timer after the storm".format(self.name),
                              start_date_time=self.scenario_start_date_time)

            self.storm_lengths.append(storm_length)
            self.window_length.append(next_storm_arrival)

        except simpy.Interrupt as interrupt:
            # here the waiting time out was interrupted by the working process, which means we are
            # done with the scope and don't need to wait anymore, but we still need to stop the last
            # wait event
            self.scope_print("Stopped waiting for this storm because :{} "
                             "(finished {})".format(interrupt, self.finished_scope))
            self.scope_print(" time now {}".format(env.now))

    def scope_print(self, message):
        """
        Make a debug message with the name of the current scope in front of it

        Parameters
        ----------
        message: str
            Message to print
        """

        self.logger.debug("{} : {}".format(self.name, message))

    def time_to_next_storm(self, n_hours_since_start):
        """
        return time in hours to the next storm with respect from the start point
        'n_hours_since_start + scenario_start'

        Parameters
        ----------
        n_hours_since_start: int
            Number of hours after the start date of the scenario from which to start looking
        scenario_start_date_time:   date/time
            Start date of the scenario

        Returns
        -------
        int:
            Number of hours it takes after the time_shift for the next storm
        """

        current_date_time = self.scenario_start_date_time + \
                            pd.Timedelta(n_hours_since_start, unit="h")

        # SWITCH_FIELD contains the change of weather status which is 1 for bad and 0 for good
        # weather. When we  go from a window (0) to a storm (1), the SWITCH_FIELD has a value +
        # 1. Here we find the first occurring +1 using the idxmax
        # (return the indices of the minimums in a array)
        next_storm_date_time = self.df_data.ix[current_date_time:, SWITCH_FIELD].idxmax()

        hours_to_next_storm = old_div((next_storm_date_time - current_date_time),
                                      pd.Timedelta(1, "h"))

        self.logger.debug("current date time vs next storm: {} -- {} ({})".format(current_date_time,
                                                                                  next_storm_date_time,
                                                                                  hours_to_next_storm))

        return hours_to_next_storm

    def time_to_next_weather_window(self, n_hours_since_start):
        """
        Return time to the next weather_window

        Parameters
        ----------
        n_hours_since_start: int
            Number of hours after the time shift from which we start looking for the next window

        Returns
        -------
        int:
            Number of hours it takes after the time_shift for the next window
        """

        current_date_time = self.scenario_start_date_time + pd.Timedelta(n_hours_since_start,
                                                                         unit="h")

        # SWITCH_FIELD contains the change of weather status which is 1 for bad and 0 for good
        # weather. When we go from a storm (1) to good weather (0), the SWITCH_FIELD has a value -1.
        # Here we find the first occurring -1 using the idxmin (return the indices of the minimums
        # in a array)
        next_window_date_time = self.df_data.ix[current_date_time:, SWITCH_FIELD].idxmin()

        # turn the date/time index value to a number of hours (integer)
        hours_to_next_window = old_div((next_window_date_time - current_date_time),
                                       pd.Timedelta(1, "h"))

        self.logger.debug(
            "current date time vs next window: {} -- {} ({})".format(current_date_time,
                                                                     next_window_date_time,
                                                                     hours_to_next_window))

        return hours_to_next_window

    def set_state_for_scope(self):
        """
        At the end of the scope we still need to store the states
        """

        # start with just set the whole scope running
        self.scope_print("Start settings states for {}".format(self.name))

        self.df_data.ix[self.start_date_time:self.end_date_time, STATE_FIELD] = STATE_WAITING

        if not self.timer.start_time_list:
            # in case we do not have any 'restart' times, it means we have started immediately and
            # run until the end
            t_start = self.full_date(self.start_of_scope)
            t_stop = self.full_date(self.end_of_scope)
            self.df_data.ix[t_start:t_stop, STATE_FIELD] = STATE_RUNNING
        else:
            # we have stored a least one start time, so
            for i_start, start_time in enumerate(self.timer.start_time_list):
                try:
                    stop_time = self.timer.stop_time_list[i_start]
                    if stop_time < start_time:
                        stop_time = self.timer.stop_time_list[i_start + 1]
                except IndexError:
                    self.logger.debug("No items in stop time list set to zero")
                    stop_time = self.end_of_scope

                t_start = self.scenario_start_date_time + pd.Timedelta(start_time, unit="h")
                t_stop = self.scenario_start_date_time + pd.Timedelta(stop_time, unit="h")

                self.scope_print(
                    "Setting state field to wait for range : {} - {}".format(start_time, stop_time))

                # update t_start to make sure what we do not stop process at t_start
                # i_loc = self.df_data[STATE_FIELD].index.get_loc(t_start - pd.Timedelta(1,
                # unit="h"), method="pad")
                # t_start = self.df_data[STATE_FIELD].index[i_loc]

                # i_loc = self.df_data[STATE_FIELD].index.get_loc(t_stop + pd.Timedelta(1,
                # unit="h"), method="bfill")
                # t_stop = self.df_data[STATE_FIELD].index[i_loc]

                self.df_data.ix[t_start:t_stop, STATE_FIELD] = STATE_RUNNING


class Scenario(object):
    """
    The Scenario class holds all the information relating to a scenario.

    Parameters
    ----------
    scenarios: obj, optional
        Reference to all scenarios. Default = None
    environmental_data_base: obj, optional
        The environmental data base reference. Default = None
    main_scenario_name: str, optional
        Name of the main scenario. Default = "Main"
    parent_name: str, optional
        Name of the parent of this scenario. Default = None
    file_base_windowed_data: str, optional
        File name of the windowed data. Default = "temp"
    reset_windowed_data: bool, optional
        Reset the data windows. Default = True

    Notes
    -----
    * The first scenario is called 'Main'. Al the other scenarios are offsprings of the Main
      scenarios
    """

    def __init__(self,
                 scenarios=None,
                 environmental_data_base=None,
                 consumable_collection=None,
                 main_scenario_name="Main",
                 parent_name=None,
                 file_base_windowed_data="temp",
                 reset_windowed_data=True):
        self.logger = get_logger(__name__)
        self.parent_name = parent_name

        if re.search("_", main_scenario_name):
            raise ValueError("Underscores in scenario names are not allowed! Found in: '{}'".format(
                main_scenario_name))

        if parent_name is None:
            self.name = main_scenario_name
        else:
            # compose the name of a scenario from the parent and its own
            self.name = "_".join([main_scenario_name, parent_name])

        self.scenarios = scenarios
        self.scenario = scenarios[main_scenario_name]
        self.environmental_data_base = environmental_data_base
        self.environmental_data_base_latest_date_time = environmental_data_base.index[-1]

        # each scenario must have a unique name. Store this name in the ordered global list
        SCENARIO_FAMILY[self.name] = len(list(SCENARIO_FAMILY.keys()))

        self.consumable_collection = consumable_collection

        # create a file base for the windowed data files
        self.file_base_windowed_data = file_base_windowed_data
        self.reset_windowed_data = reset_windowed_data

        # create date time index based on the environmental data base
        self.date_time_index = [pd.Timestamp(index) for index in self.environmental_data_base.index]

        self.start_date_time = None
        self.end_date_time = None
        self.total_time = None

        # the current scope name keeps track of the scope we are currently processing
        self.scopes = OrderedDict()

        # this pointer keeps track of the currently running scope of this scenario
        self.current_scope_name = None

        self.process_consumables = None
        self.process_main = None
        self.waiting_for_weather = False
        self.waiting_for_parallel_processes = False
        self.number_of_scopes_left = None

        # when creating a scenario, we have to prepare the scenario by finding the weather windows
        self.prepare_scenario()

    def process_scenario(self, env, start_date_time):
        """
        After preparing the scenario, this routine can be called per start time

        Parameters
        ----------
        env: obj
            Environment object from Simpy  environment
        start_date_time: DateTime
            start date/time of this scenario
        """

        self.start_date_time = pd.Timestamp(start_date_time)

        self.logger.info("Running scenario: '{}'".format(self.scenario["description"]))

        for scope_name, scope in self.scopes.items():
            self.logger.debug("Starting scope {}".format(scope_name))

            # for each new scope we need to store the start date_time of its holding scenario
            scope.scenario_start_date_time = self.start_date_time

            scope.working_process = env.process(scope.process_scope(env))

            for par_name, par_scenario in scope.parallel_scenarios.items():
                self.logger.debug("Also submitting parallel scope {}".format(par_name))
                par_scenario.process_scenario(env, start_date_time)

            env.run()

            # at the end of each scope we have to set the idle time by storing the
            latest_end_date_time = scope.end_date_time
            for par_name, par_scenario in scope.parallel_scenarios.items():
                if par_scenario.end_date_time > latest_end_date_time:
                    latest_end_date_time = par_scenario.end_date_time

            if latest_end_date_time > scope.end_date_time:
                scope.df_data[scope.end_date_time:latest_end_date_time] = STATE_IDLE
            for par_name, par_scenario in scope.parallel_scenarios.items():
                if latest_end_date_time > par_scenario.end_date_time:
                    for par_scope_name, par_scope in par_scenario.scopes.items():
                        if latest_end_date_time > par_scope.end_date_time:
                            par_scope.df_data[
                            par_scope.end_date_time:latest_end_date_time] = STATE_IDLE

        # store the end date time of the last scope
        self.end_date_time = scope.end_date_time
        self.total_time = scope.end_date_time - self.start_date_time

    def scenario_scope_file_name(self, scope_name):
        """
        Create a unique key which can be used for

        Parameters
        ----------
        scope_name: str
            Name of the scope to build a key for

        Notes
        -----
        * Based on the current scenario + scope key, create a unique key which can be used for
          creating file names

        Returns
        -------
        tuple (msg_pack_filename, info_filename)
            Name of the unique file belong to this scenario/scope.
            Both the message pack file and the information file (containing all threshold) are given
        """

        file_key = "_".join([self.name, scope_name])
        file_base = "_".join([self.file_base_windowed_data, file_key]) + ".msg_pack"
        msg_pack_filename = file_base + ".msg_pack"
        info_filename = file_base + ".yaml"
        self.scopes[scope_name].msg_pack_filename = msg_pack_filename
        self.scopes[scope_name].info_file_name = info_filename
        return msg_pack_filename, info_filename

    def check_if_scope_settings_have_changed(self, info_file, scope_name):
        """
        Read the scope settings yaml file.

        Parameters
        ----------
        info_file: str
            Name of the info file
        scope_name: str
            Name of the scope

        Returns
        -------
        bool:
            Flag to indicate the the scope settings have changed. If it does not exist,
            or if it exists but has a difference content than the current scope settings,
            return True
        """
        scope_settings_have_changed = False
        try:
            with open(info_file, "r") as stream:
                info_dict = yaml.load(stream=stream, Loader=yamlordereddictloader.Loader)
            # successfully read the info_dict. No loop over the key to see if one of them has
            # changed
            for key in SCOPE_FIELDS_TO_DUMP_FOR_RESTART:
                # get the value of this key from the scope opbject using getattr
                attr_value = getattr(self.scopes[scope_name], key)
                self.logger.debug(
                    "Comparing {}: {} with {}".format(key, info_dict[key], attr_value))
                if info_dict[key] != attr_value:
                    scope_settings_have_changed = True
                    self.logger.debug("Scope settings for {} have changed for key {} from {} -> {}"
                                      "".format(key, scope_name, info_dict[key], attr_value))
        except IOError:
            scope_settings_have_changed = True
            self.logger.debug(
                "Failed reading the scope settings for {}. No problem, regenerate the windowed data"
                "".format(scope_name))

        # store the results in the scope as well for later usage with the plotting
        self.scopes[scope_name].has_changed = scope_settings_have_changed

        return scope_settings_have_changed

    def write_scope_settings_to_file(self, info_file, scope_name):
        # also dump the settings of this scope to the yaml file
        dump_settings = dict()
        for key in SCOPE_FIELDS_TO_DUMP_FOR_RESTART:
            # store the required scope settings which need to be checked into a dictionary
            dump_settings[key] = getattr(self.scopes[scope_name], key)

        # write the dictionary to file
        with open(info_file, "w") as stream:
            self.logger.debug("Dumping the scope settings to {}".format(info_file))
            yaml.dump(dump_settings, stream, default_flow_style=False)

    @staticmethod
    def get_scenario_names(scope_properties, scenario_type):
        """ 
        Get the scenario names from the scope properties taking into account that it is given as
        a single value or a list.

        Parameters
        ----------
        scenario_type: {'parallel', 'conditional'}
            Type of the scenario, either 'parallel' or 'conditional'

        Notes
        -----

        A scenario can be either one scope of a list of scopes. So you can define it as::

            parallel: scenario_name

        Or you can define it as a list as::

            parallel:
            - scenarion_name1
            - scenarion_name2
        
        In case that no value is given for parallel an empty list is return
        """
        scenario_names = read_value_from_dict_if_valid(scope_properties, scenario_type, [])
        if scenario_names is None:
            # in case no value is given, just set an empty list
            scenario_names = []
        elif isinstance(scenario_names, basestring):
            #  in case only one scenario is given as a string, add this string to a list
            scenario_names = [scenario_names]

        return scenario_names

    def prepare_scenario(self):
        """
        Prepare the scenario by getting all threshold of the scopes.

        Notes
        -----

        * All windowed data is written to temporary output file which is read the next time we run
          this routine.
        * In case any of the threshold is changed, use the --reset_wow argument to force to read
          generate the windowed data from the environmental data base
        """

        self.logger.info("Preparing scenario: '{}'".format(self.scenario["description"]))
        process_steps = self.scenario["process_steps"]

        for scope_name, scope_properties in process_steps.items():

            self.logger.info(
                "Start preparing scenario {}  scope : {}".format(self.name, scope_name))

            # store the current scope name so we can use it in the update_window_scope routine
            self.current_scope_name = scope_name

            # read the properties of this scope
            description = read_value_from_dict_if_valid(scope_properties, "description")
            units = read_value_from_dict_if_valid(scope_properties, "units")
            value = read_value_from_dict_if_valid(scope_properties, "value")
            window = read_value_from_dict_if_valid(scope_properties, "window", 0)
            gap = read_value_from_dict_if_valid(scope_properties, "gap", 0)
            delay = read_value_from_dict_if_valid(scope_properties, "delay", 0)
            thresholds = read_value_from_dict_if_valid(scope_properties, "thresholds", dict())

            conditional_scenario_names = self.get_scenario_names(scope_properties, "conditional")
            parallel_scenario_names = self.get_scenario_names(scope_properties, "parallel")

            if units in list(self.consumable_collection.items.keys()):
                self.logger.debug("We have found a consumable {}".format(units))
                consumable_name = self.consumable_collection.items[units]
            else:
                consumable_name = None

            # create a new scope entry with a Scope object to store all the properties
            self.scopes[scope_name] = Scope(scope_name, value,
                                            description=description,
                                            units=units,
                                            thresholds=thresholds,
                                            window=window,
                                            gap=gap,
                                            delay=delay,
                                            date_time_index=self.date_time_index,
                                            has_changed=self.reset_windowed_data,
                                            scenarios=self.scenarios,
                                            environmental_data_base=self.environmental_data_base,
                                            consumable_collection=self.consumable_collection,
                                            parallel_scenario_names=parallel_scenario_names,
                                            conditional_scenario_names=conditional_scenario_names,
                                            consumable_name=consumable_name,
                                            file_base_windowed_data=self.file_base_windowed_data,
                                            reset_windowed_data=self.reset_windowed_data
                                            )

            # check here if we have already windowed the data before
            msg_pack_file, info_file = self.scenario_scope_file_name(scope_name)

            if not self.reset_windowed_data:
                # only check if the scope settings have changed if we are not resetting the data
                scope_settings_have_changed = self.check_if_scope_settings_have_changed(info_file,
                                                                                        scope_name)
            else:
                scope_settings_have_changed = None

            if os.path.exists(msg_pack_file) and not (
                    self.reset_windowed_data or scope_settings_have_changed):
                # in case the msg_pack file exist and we are not requesting a reset and the scope
                # are unchanged, read the data from the msg_pack file
                self.logger.debug(
                    "Reading the previously windowed data from: {}".format(msg_pack_file))
                self.scopes[scope_name].df_data = pd.read_msgpack(msg_pack_file)
            else:
                # we can not read the windowed data from the message pack file due to one of the
                # reasons above: recalculate the windowed data and write the results to the msg_file
                self.logger.info(
                    "Updating the thresholds scope : {}".format(self.current_scope_name))
                self.update_current_scope_windows()

                # dump the windowed data to a unique file
                self.logger.debug("Writing to the windowed data file: {}".format(msg_pack_file))
                self.scopes[scope_name].df_data.to_msgpack(msg_pack_file)

                # dump the scope settings to a yaml file for later comparision
                self.write_scope_settings_to_file(info_file, scope_name)

    def consumable_control(self, env, scope):

        while True:

            if scope.consumable is not None:
                if scope.consumable.container.level >= scope.value:
                    yield self.leaving_the_process(scope)

            # update every 1 hours
            yield env.timeout(1)

    def leaving_the_process(self, scope):

        self.logger.warning("Now here. What's next ?")

    def update_consumable(self, env, scope):

        while True:
            if scope.value < 0:
                delta_consumable = scope.window * scope.consumable.replenish_rate
                self.logger.debug(
                    "Putting into the stock {} : {} @ {}".format(scope.consumable.name,
                                                                 delta_consumable,
                                                                 env.now))
                # for negative values the consumables are being added to the container so take the
                # replenish rate
                yield scope.consumable.container.put(delta_consumable)

            elif scope.value > 0:
                delta_consumable = scope.window * scope.consumable.consumption_rate
                self.logger.debug(
                    "Taking out of the stock {} : {} @ {}".format(scope.consumable.name,
                                                                  delta_consumable,
                                                                  env.now))
                yield scope.consumable.container.get(delta_consumable)

            self.logger.debug("After the time out")

    def run_parallel_processes(self, scope):
        """
        run all the parallel process of this scope
        :param scope: a refernce to the scope object
        :return:
        """
        for scenario_name, parallel_scenario in scope.parallel_scenarios.items():
            self.logger.info("Start Parallel scenario {} of scope {} at time {}"
                             "".format(scenario_name, scope.name, scope.start_date_time))
            env = simpy.Environment()
            parallel_scenario.process_scenario(env, scope.start_date_time)
            env.run()
            self.logger.info("Finalized parallel scenario at {} ({} h)"
                             "".format(parallel_scenario.end_date_time,
                                       parallel_scenario.total_time))

    def get_limiting_process_time(self, scope):
        """
        get the time of the slowest of a series of parallel processes belonging to scope
        :param scope: a reference to the scope object
        :return: max_time_left
        """
        max_time_left = 0
        limiting_parallel_scenario = 0
        self.logger.debug("Starting wait for all othe processes of {}".format(scope.name))
        for scenario_name, parallel_scenario in scope.parallel_scenarios.items():
            self.logger.debug("Get time diff from {} {}".format(scope.end_date_time,
                                                                parallel_scenario.end_date_time))
            time_left = old_div((parallel_scenario.end_date_time - scope.end_date_time),
                                pd.Timedelta(1, "h"))
            self.logger.debug("time left for scope {} {} - {} = {} h"
                              "".format(scenario_name, scope.end_date_time,
                                        parallel_scenario.end_date_time, time_left))
            if time_left > max_time_left:
                max_time_left = time_left
                limiting_parallel_scenario = scenario_name

        self.logger.debug(
            "Found max time {} for process {}".format(max_time_left, limiting_parallel_scenario))

        return max_time_left

    def working_on_scenario(self, env):
        """ 
        submit all the steps of this scenario 
        obsolete. We can delete this after a while
        """

        print_banner("Start scenario {}: '{}' ".format(self.name, self.scenario["description"]),
                     top_symbol="+")

        self.number_of_scopes_left = len(list(self.scopes.keys()))

        for scope_name, scope in self.scopes.items():

            self.logger.debug("Storing current scope: {} ".format(scope))
            self.current_scope_name = scope_name

            # initialise the current scope time list and start time
            start_time = env.now
            start_date_time = self.start_date_time + pd.Timedelta(start_time, "h")

            scope_name = self.current_scope_name
            scope = self.scopes[scope_name]
            duration = scope.value

            print_banner("Start scope {} at time {}".format(scope_name, start_date_time))

            # store the start date time of this scope
            scope.start_date_time = start_date_time

            # run all the parallel processes of this scope
            self.run_parallel_processes(scope)

            # initialise the time for this scope to the total duration of the current scope
            time_left_for_this_scope = duration

            self.logger.info("Start process step {} at {:.1f} for left {:.1f} of {:.1f}"
                             "".format(scope_name, env.now, time_left_for_this_scope, duration))

            # as long as there is time left, keep doing this
            while time_left_for_this_scope:
                try:

                    # at this point time_left_for_this_scope keeps being updated
                    start_time = env.now
                    if start_time not in scope.start_time_list:
                        scope.start_time_list.append(start_time)
                    yield env.timeout(time_left_for_this_scope)
                    end_time = env.now

                    # get the state name for this scope
                    previous_time = scope.start_time_list[-1]
                    date_time_last_state = self.start_date_time + pd.Timedelta(previous_time, "h")
                    date_time_this_state = self.start_date_time + pd.Timedelta(end_time, "h")
                    self.logger.debug("Storing RUNNING {} from {} to {}"
                                      "".format(STATE_RUNNING, date_time_last_state,
                                                date_time_this_state))
                    # for this scope store the current state to be running
                    scope.df_data.ix[date_time_last_state:date_time_this_state,
                    STATE_FIELD] = STATE_RUNNING

                    end_date_time = self.start_date_time + pd.Timedelta(end_time, "h")
                    scope.end_date_time = end_date_time
                    scope.total_time = end_date_time - scope.start_date_time
                    if end_time not in scope.stop_time_list:
                        scope.stop_time_list.append(end_time)

                    time_left_for_this_scope = 0
                    self.number_of_scopes_left -= 1

                    self.logger.debug("No more time left for this scope {} n_scopes_left={}. "
                                      "Go to next {} ({})".format(scope_name,
                                                                  self.number_of_scopes_left,
                                                                  end_date_time, end_time))

                except simpy.Interrupt:
                    # an interrupt is received, which means we have to wait for weather here
                    self.waiting_for_weather = True
                    interrupt_time = env.now

                    scope_name = self.current_scope_name
                    scope = self.scopes[scope_name]

                    self.logger.debug(
                        "Interrupting scope {} of scenario {}".format(scope_name, self.name))

                    # get the state name for this scope
                    previous_time = scope.start_time_list[-1]
                    date_time_last_state = self.start_date_time + pd.Timedelta(previous_time, "h")
                    date_time_this_state = self.start_date_time + pd.Timedelta(interrupt_time, "h")
                    scope.df_data.ix[date_time_last_state:date_time_this_state,
                    STATE_FIELD] = STATE_RUNNING

                    interrupt_date_time = self.start_date_time + pd.Timedelta(interrupt_time, "h")
                    self.logger.info(
                        "{} stopped at {} ({:.1f})".format(scope_name, interrupt_date_time,
                                                           interrupt_time))
                    scope.stop_time_list.append(interrupt_time)

                    # update the time left
                    time_left_for_this_scope -= env.now - start_time

                    # update consumables
                    if scope.consumable is not None:
                        delta_time = interrupt_time - previous_time
                        if scope.value < 0:
                            delta_consumable = delta_time * scope.consumable.replenish_rate
                            if delta_consumable > 0:
                                self.logger.debug("Putting into the stock {} : {}/{} @ {}"
                                                  "".format(scope.consumable.name, delta_consumable,
                                                            scope.consumable.container.level,
                                                            env.now))
                            yield scope.consumable.container.put(delta_consumable)
                        elif scope.value > 0:
                            delta_consumable = delta_time * scope.consumable.consumption_rate
                            if delta_consumable > 0:
                                self.logger.debug("Taking out of the stock '{}' : {}/{} @ {}"
                                                  "".format(scope.consumable.name, delta_consumable,
                                                            scope.consumable.container.level,
                                                            env.now))
                                yield scope.consumable.container.get(delta_consumable)
                                self.logger.debug("Are we here ?")

                    if self.number_of_scopes_left > 0:
                        yield env.timeout(self.time_to_next_weather_window(env))

                    restart_time = env.now

                    scope_name = self.current_scope_name
                    scope = self.scopes[scope_name]

                    self.logger.debug(
                        "Restaring scope {} of scenario {}".format(scope_name, self.name))

                    # get the state name for this scope
                    previous_time = scope.stop_time_list[-1]
                    date_time_last_state = self.start_date_time + pd.Timedelta(previous_time, "h")
                    date_time_this_state = self.start_date_time + pd.Timedelta(restart_time, "h")
                    self.logger.debug(
                        "Storing WAITING state from {} to {}".format(date_time_last_state,
                                                                     date_time_this_state))
                    scope.df_data.ix[date_time_last_state:date_time_this_state,
                    STATE_FIELD] = STATE_WAITING

                    self.logger.info(
                        "{} starting again at {} ({:.1f})".format(scope_name, date_time_this_state,
                                                                  restart_time))
                    scope.start_time_list.append(restart_time)

                    self.waiting_for_weather = False

            if scope.parallel_scenarios:
                # only wait for parallel processes if there are any
                self.waiting_for_parallel_processes = True
                time_left_to_wait_for_parallel = self.get_limiting_process_time(scope)
                self.logger.info(
                    "Done with current scope {} at {}. "
                    "Now wait for all parallel processes to finish {}"
                    "".format(scope.name, self.start_date_time + pd.Timedelta(env.now, "h"),
                              time_left_to_wait_for_parallel))

                idle_wait_start = env.now
                if idle_wait_start not in scope.idle_start_time_list:
                    scope.idle_start_time_list.append(idle_wait_start)

                if time_left_to_wait_for_parallel > 0:
                    yield env.timeout(time_left_to_wait_for_parallel)

                    self.waiting_for_parallel_processes = False
                    idle_wait_end = env.now
                    if idle_wait_end not in scope.idle_end_time_list:
                        scope.idle_end_time_list.append(idle_wait_end)

                    previous_time = scope.stop_time_list[-1]
                    date_time_last_state = self.start_date_time + pd.Timedelta(previous_time, "h")
                    date_time_this_state = self.start_date_time + pd.Timedelta(idle_wait_end, "h")
                    self.logger.debug(
                        "Storing WAITING state from {} to {}".format(date_time_last_state,
                                                                     date_time_this_state))
                    scope.df_data.ix[date_time_last_state:date_time_this_state,
                    STATE_FIELD] = STATE_IDLE

        end_time = env.now
        self.end_date_time = self.start_date_time + pd.Timedelta(env.now, unit="h")

        if self.end_date_time >= self.environmental_data_base_latest_date_time:
            self.logger.warning("The simulation was running out of the range of the data base: {}"
                                "".format(self.environmental_data_base_latest_date_time))
        self.total_time = self.end_date_time - self.start_date_time
        self.logger.info("Done at the end {} ".format(end_time))
        env.timeout(0)
        self.logger.debug("Goodbye {}".format(env.now))

    def update_current_scope_windows(self):
        """

        Set the mask and weather windows of the current scope based on the threshold and
        weather windows

        Notes
        -----
        * All windows smaller than the required window length are removed
        """

        scope_name = self.current_scope_name
        scope = self.scopes[scope_name]

        if scope.units == "hours" or scope.units is None:
            # normal time units
            window_in_hours = scope.window
        else:
            # we are dealing with some other units
            if scope.consumable is not None:
                window_in_hours = scope.consumable.window_in_hours(scope.window, scope.value)
            else:
                raise ValueError(
                    "Not implemented yet for this unit {} {}".format(scope.name, scope.units))

        self.logger.debug("Initializing mask and state of {}".format(scope_name))
        scope.df_data[MASK_FIELD] = False  # initialise a mask field to False (no storm anywhere)
        scope.df_data[STATE_FIELD] = np.nan

        # loop over the threshold fields and set m_name to true whenever we exceed the threshold
        for quantity, threshold in scope.thresholds.items():
            # create a new mask. For any of the environmental data base thresholds is true,
            # set mask true
            mask_for_quantity = self.environmental_data_base[quantity] >= threshold
            scope.df_data[MASK_FIELD] = mask_for_quantity.values | scope.df_data[MASK_FIELD].values

        # the change from a window (false) to a storm (true) is +1, from storm to window is -1.
        # Store the values
        change_of_mask = np.diff(scope.df_data[MASK_FIELD].values.astype(int))
        scope.df_data[SWITCH_FIELD] = np.append(np.array([0]), change_of_mask)

        # now find all the windows and remove the windows which are too short

        # set the start date at the beginning of a weather window
        current_date_time = scope.df_data.index[0]
        if scope.df_data.ix[current_date_time, MASK_FIELD]:
            # the first date is in a storm.  Got to the first window
            current_date_time = scope.df_data[SWITCH_FIELD].idxmin()

        if scope.df_data[MASK_FIELD].any():
            find_windows = True
        else:
            find_windows = False

        while find_windows:
            # we are at the start of a weather window here
            # Get the next storm of the current date/time by looking for the first +1 in the
            # SWITCH_FIELD
            next_storm_date_time = scope.df_data.ix[current_date_time:, SWITCH_FIELD].idxmax()
            # subtract the gap time from the next storm that in case we need some time to clear up
            # get the length of the window
            window_length = old_div((next_storm_date_time - current_date_time),
                                    pd.Timedelta(1, "h"))
            if window_length < window_in_hours:
                # this window is too short. Remove it by setting the mask value to True (is storm)
                scope.df_data.ix[current_date_time: next_storm_date_time, MASK_FIELD] = True
            else:
                # subtract the gap from the next storm date. Make sure that the gap is not greater
                # then the window
                gap_length = scope.gap if window_length >= scope.gap else window_length
                next_storm_minus_gap_date_time = next_storm_date_time - pd.Timedelta(gap_length,
                                                                                     "h")
                # we may leave the window, but must remove the gap before the storm
                scope.df_data.ix[next_storm_minus_gap_date_time: next_storm_date_time,
                MASK_FIELD] = True

            # find the next window with respect from the next storm
            next_window_date_time = scope.df_data.ix[next_storm_date_time:, SWITCH_FIELD].idxmin()
            if next_window_date_time > current_date_time:
                # update the current date to the next window and continue
                current_date_time = next_window_date_time
            else:
                # if the next window date/time is the same as the current date time, there are no
                # windows left.
                self.logger.debug(
                    "Found last window at {}. Leavig the loop".format(current_date_time))
                break

        # we have updated the mask field indicating all the storms with True, including the windows
        # which are too short. Now update the weather switch field so we can use it without
        # checking the window length
        self.logger.debug("Done with filling the smallest windows. Now reset the diff")
        change_of_mask = np.diff(scope.df_data[MASK_FIELD].values.astype(int))
        # prepend the zero in order to set the change -after- the step, which is consistent with the
        # back search pad of i_loc in the check_status routine
        scope.df_data[SWITCH_FIELD] = np.append(np.array([0]), change_of_mask)

    def waiting_for_processes(self, env):
        """
        Send an interrupt when we are waiting for other unfinished jobs
        """
        while self.number_of_scopes_left > 0:

            scope_name = self.current_scope_name
            scope = self.scopes[scope_name]

            # get the longest waiting time of all the parallel processes belonging to this scope
            time_max = self.get_limiting_process_time(scope)

            # wait for the delayed time of the parallel process
            yield env.timeout(time_max)

            if self.number_of_scopes_left > 0 and not self.waiting_for_parallel_process:
                # only interrupt the process when it was not waiting already.
                self.logger.debug("Going to interrupt at {} from {} ({}) because {}"
                                  "".format(self.start_date_time + pd.Timedelta(env.now, "h"),
                                            self.start_date_time + pd.Timedelta(env.now + time_max,
                                                                                "h"),
                                            time_max, self.waiting_for_parallel_processes))
                self.process_main.interrupt()

    def weather_down(self, env):
        """
        Send an interrupt each time we are facing bad weather
        """

        while self.number_of_scopes_left > 0:

            scope_name = self.current_scope_name
            scope = self.scopes[scope_name]

            if scope.initialize_state:
                # if the initialise_state flag is true, we want to do one check of the state
                # immediately as t=0 to see if we are not starting in a sea state already
                self.logger.debug(
                    "Setting delta_t to zero for scope {} of scenario {}".format(scope_name,
                                                                                 self.name))
                delta_t = 0
                scope.initialize_state = False
            else:
                # normal sea state checks are done every hour
                delta_t = 1

            # slowly progress in time
            yield env.timeout(delta_t)

            # check the status of the current process. If we exceed the limits, check_status
            # return true and we are going to interrupt the running process
            is_storm, out_of_date_range, is_storm_ref_date = self.check_status(env)

            if is_storm and self.number_of_scopes_left > 0 and not self.waiting_for_weather and \
                    not self.waiting_for_parallel_processes:
                # only interrupt the process when it was not waiting already.
                self.logger.debug(
                    "Going to interrupt at {} from {} ({}) because is_storm={} and wow={}"
                    "".format(self.start_date_time + pd.Timedelta(env.now, "h"), is_storm_ref_date,
                              env.now, is_storm, self.waiting_for_weather))
                self.process_main.interrupt()

    def check_status(self, env):
        """
        Check the status of the current environment

        Returns
        -------
        tuple (is_storm, out_of_data_range, is_storm_ref_date)
            Tuple with threeflags indicating the status :

            1. is_storm: We are in a storm
            2. out_of_data_range: We are out the date range
            3. is_storm_ref_date: DateTime reference of the storm
        """
        current_date_time = self.start_date_time + pd.Timedelta(env.now, unit="h")
        scope_name = self.current_scope_name
        scope = self.scopes[scope_name]

        i_loc = scope.df_data[MASK_FIELD].index.get_loc(current_date_time, method="pad")
        is_storm = scope.df_data[MASK_FIELD].iloc[i_loc]
        is_storm_ref_date = scope.df_data[MASK_FIELD].index[i_loc]

        out_of_data_range = False
        if current_date_time is not None and self.end_date_time is not None:
            # in case the end_date_time has been set and we are exceeding the end: set storm to
            # False to prevent the interrupt
            if current_date_time > self.end_date_time:
                self.logger.debug("We are out of date range at {} {}".format(current_date_time,
                                                                             self.end_date_time))
                out_of_data_range = True
        return is_storm, out_of_data_range, is_storm_ref_date

    def make_report(self):
        """
        Create a dump to screen of al the info
        :return:
        """
        msg = "{:20s} : {}"
        msg2 = "{:20s} : {} ({} h)"
        self.logger.info(msg.format("Scenario", self.name))
        self.logger.info(msg.format("Description", self.scenario["description"]))
        process_steps = self.scenario["process_steps"]
        for scope_name, scope in self.scopes.items():
            self.logger.info("++++++++++++++++++ Scope {} ++++++++++++++++".format(scope_name))
            self.logger.info(msg.format("Start date/time", scope.start_date_time))
            for i_time, start_time in enumerate(scope.timer.start_time_list):
                start_date_time = self.start_date_time + pd.Timedelta(start_time, "h")
                self.logger.debug(msg.format("Restart {:02d}".format(i_time),
                                             "{} ({} h)".format(start_date_time, start_time)))
            for i_time, stop_time in enumerate(scope.timer.stop_time_list):
                stop_date_time = self.start_date_time + pd.Timedelta(stop_time, "h")
                self.logger.debug(msg.format("Interrupt {:02d}".format(i_time),
                                             "{} ({} h)".format(stop_date_time, stop_time)))
            self.logger.info(msg.format("End date/time", scope.end_date_time))
            self.logger.info(msg2.format("Total time", scope.total_time, scope.total_time_in_hours))
        self.logger.info("========================== Global Dates ==========================")
        self.logger.info(msg.format("Start date/time", self.start_date_time))
        self.logger.info(msg.format("End date/time", self.end_date_time))
        self.logger.info(msg2.format("Total time", self.total_time,
                                     old_div(self.total_time, pd.Timedelta(1, "h"))))


class ScenarioSequence(object):
    """
    Class to hold a sequence  of scenarios

    Parameters
    ----------
    environmental_data_base_file_name: str
        Name of the environmental data file
    environmental_temp_out_file: str, optional
        Temporary name of the environmental data cache file. Default = "env_temp.msg_pack"
    environmental_data_fields: obj, optional
        Data Frame with all the data fields
    environmental_select_fields: bool, optional
        Make selection of the data fields used . Default = False
    reset_environmental_data: bool, optional
        Reset the environmental data fields. Default = False
    reset_wow_thresholds: bool, optional
        Reset the thresholds of the environmental data Default =False
    output_directory: str, optional
        Name of the output directory. Default = "." (current directory)
    results_file_base: str, optional
        Name of the results file. Default = "scoda"
    temporary_directory: str, optional
        Name of the temporary directory. Default = "." (current directory)
    save_images: bool, optional
        Save the images to file. Default = False
    show_plots: bool, optional
        Show the plot. Default = False
    progress_bar: bool, optional
        Show a progress bar in stead of message output. Default = False
    end_date_time: Date time, optional
        End date/time to process. Default = None (process until the end)
    window_title: str, optional
        Name of the window of the plots. Default = "Default window title"
    figsize: str, optional
        Size of the plots. Default = (16, 10)
    marker_size: int, optional
        Size of the markers. Default = 0 (don't show the markers at all)
    image_file_type: str, optional
        Extension of the images Default = ".png"
    legend_x_position: float, optional
        X position of th legend as a fraction of the x axis. Default = 1.1
    legend_y_position: float, optional
        Y position of th legend as a fraction of the y axis. Default = 1.0
    legend_title: str, optional
        Title to add above the legend. Default = None (no title is added)
    legend_font_size: int, optional
        Font size of the legend. Default = 12
    x_label: str, optional
        Label to put at the x-axis. Default = "Date"
    y_label: str, optional
        Label to put at the y-axis. Default = "Scenario"
    sub_plot_labels": list, optional
        List of labels to add to the subplots. Default = [] (No labels are added)
    heading_colorbar: bool, optional
        Add a heading color barl. Default = False
    n_bins_heading_colorbar: int, optional
        Number of bins used for the contour color bar. Default = 200
    combine_plot_lines: bool, optional
        Add multple plot lines into one plot. Default = False
    scenario_dictionary: dict, optional
        Dictionary containing the parents scenerarios. Default = None
    date_range: list, optional
        A list of date/time to give the range of processing. Default = list()

    number_of_days_to_plot: int,  optional
        Number of days to plot. Default = None, ie. plot all dates
    number_of_days_to_plot_before: int,  optional
        Number of days to add before the start date. Default = None
    number_of_days_to_plot_after: int,  optional
        Number of days to add after the start date. Default = None
    number_of_ancestors_to_show: int, optional
        Maximum number of ancestor to show in the plot. Default = None
    """

    def __init__(self, *args, **kwargs):
        # get the logger name and store
        self.logger = get_logger(__name__)

        self.environment = simpy.Environment()

        # now get the key word argument needed at this class level
        self.environmental_data_base_file_name = kwargs.pop("environmental_data_base_file_name",
                                                            None)
        self.environmental_temp_out_file = kwargs.pop("environmental_temp_out_file",
                                                      "env_temp.msg_pack")
        self.environmental_data_fields = kwargs.pop("environmental_data_fields", None)
        self.environmental_select_fields = kwargs.pop("environmental_select_fields", False)
        self.reset_environmental_data = kwargs.pop("reset_environmental_data", False)
        self.reset_wow_thresholds = kwargs.pop("reset_wow_thresholds", False)

        self.output_directory = kwargs.pop("output_directory", ".")
        self.results_file_base = kwargs.pop("results_file_base", "scoda")
        self.temporary_directory = kwargs.pop("temporary_directory", ".")
        self.save_images = kwargs.pop("save_images", False)
        self.show_plots = kwargs.pop("show_plots", False)
        self.progress_bar = kwargs.pop("progress_bar", False)

        # plot arguments
        self.end_date_time = kwargs.pop("end_date_time", None)
        self.window_title = kwargs.pop("window_title", "Default window title")
        self.figsize = kwargs.pop("figsize", (16, 10))
        self.marker_size = kwargs.pop("marker_size", 0)
        self.image_file_type = kwargs.pop("image_file_type", ".png")
        self.legend_x_position = kwargs.pop("legend_x_position", 1.1)
        self.legend_y_position = kwargs.pop("legend_y_position", 1.0)
        self.legend_title = kwargs.pop("legend_title", None)
        self.legend_font_size = kwargs.pop("legend_font_size", 12)
        self.x_label = kwargs.pop("x_label", "Date")
        self.y_label = kwargs.pop("y_label", "Scenario")
        self.sub_plot_labels = kwargs.pop("sub_plot_labels", [])
        self.heading_colorbar = kwargs.pop("heading_colorbar", False)
        self.n_bins_heading_colorbar = kwargs.pop("n_bins_heading_colorbar", 200)
        self.environmental_combine_plot_lines = kwargs.pop("combine_plot_lines", False)

        # scenarios
        self.scenario_dictionary = kwargs.pop("scenario_dictionary", None)
        self.date_range_input = kwargs.pop("date_range", None)
        self.number_of_days_to_plot = kwargs.pop("number_of_days_to_plot", None)
        self.number_of_days_to_plot_before = kwargs.pop("number_of_days_to_plot_before", None)
        self.number_of_days_to_plot_after = kwargs.pop("number_of_days_to_plot_after", None)
        self.number_of_ancestors_to_show = kwargs.pop("number_of_ancestors_to_show", None)

        # consumables
        self.consumable_dictionary = kwargs.pop("consumable_dictionary", OrderedDict())

        self.n_scenarios = len(list(self.scenario_dictionary.keys()))
        # prepend the output directory
        if self.output_directory != ".":
            self.results_file_base = os.path.join(self.output_directory, self.results_file_base)

        # this file name base is going to be used to create the file names of the windowed data
        self.windowed_temp_out_file_base = os.path.splitext(self.environmental_temp_out_file)[0]
        if self.temporary_directory != ".":
            self.windowed_temp_out_file_base = os.path.join(self.temporary_directory,
                                                            self.windowed_temp_out_file_base)

        # get the required data fields from the environmental data field section
        self.required_environmental_data_fields = []
        for key, properties in self.environmental_data_fields.items():
            if properties["required"]:
                self.required_environmental_data_fields.append(key)
            units = read_value_from_dict_if_valid(properties, "units")
            if units is not None:
                properties["units_label"] = "[{}]".format(units)
            else:
                properties["units_label"] = ""

        # get the plotted data fields from the environmental data field section
        self.plotted_environmental_data_fields = []
        for key, properties in self.environmental_data_fields.items():
            if properties["show_plot"]:
                self.plotted_environmental_data_fields.append(key)

        # list the fields which are using within the class
        self.environmental_data_base = None

        # read the environmental data to start with
        self.read_environmental_data_base()

        # select only the date from the date range that are in common with the environmental data
        # dates
        self.date_range = pd.concat(
            [self.environmental_data_base, pd.DataFrame(index=self.date_range_input)],
            axis=1, join="inner").index

        self.logger.debug("date range input {}".format(self.date_range_input))
        self.logger.debug("date range new {}".format(self.date_range))
        n_dates_input = self.date_range_input.values.size
        n_dates_new = self.date_range.values.size
        if n_dates_new < n_dates_input:
            self.logger.info("Not all date/time are available in the environmental data base: "
                             "reducing # of date {} -> {} ".format(n_dates_input, n_dates_new))

        # initialise the consumables. It is an empty collection if we dont have consumables
        self.consumable_collection = ConsumableCollection(self.environment,
                                                          self.consumable_dictionary)

        # initialise the scenario
        self.logger.debug("Initialize all scenarios and windowing data")
        self.scenario = Scenario(self.scenario_dictionary, self.environmental_data_base,
                                 consumable_collection=self.consumable_collection,
                                 file_base_windowed_data=self.windowed_temp_out_file_base,
                                 reset_windowed_data=self.reset_wow_thresholds)

        # some plot settings
        self.n_cols = 1
        self.n_rows = 2
        self.extra_artists = list()
        self.figure, self.axis = None, None

    def read_environmental_data_base(self):
        """
        Read the environmental data base.

        Notes
        -----
        * After the first read it stores the data into a msg_pack file which will be loaded the next
          read wht reset_data is not True to speed things up
        """
        self.logger.info("Starting reading environmental data base")
        if os.path.exists(self.environmental_temp_out_file) and not self.reset_environmental_data:
            self.logger.info(
                "Reading from temporary msg_pack file {}".format(self.environmental_temp_out_file))
            data = pd.read_msgpack(self.environmental_temp_out_file)
        else:
            self.logger.info(
                "Reading from environment file {}".format(self.environmental_data_base_file_name))
            sequence_tool_data = SequenceToolEnvironment(
                filename=self.environmental_data_base_file_name)
            sequence_tool_data.list_properties()
            self.logger.info(
                "Writing to temporary msg_pack file {}".format(self.environmental_temp_out_file))
            if self.environmental_select_fields:
                self.logger.debug(
                    "Selecting columns {}".format(self.required_environmental_data_fields))
                data = sequence_tool_data.data[self.required_environmental_data_fields]
            else:
                data = sequence_tool_data.data
            if self.environmental_temp_out_file is not None:
                data.to_msgpack(self.environmental_temp_out_file)

        # set the pandas data frame to be the environmental data base
        self.environmental_data_base = data

    def init_scenario_plot(self):
        """
        Initialise the scenario plot
        """

        # this is going to hold all the artist we add in order to get a proper tight layout
        self.extra_artists = list()

        self.logger.debug("Initialise the plots")

        if self.environmental_combine_plot_lines:
            self.n_rows = 2
        else:
            self.n_rows = 1 + len(self.plotted_environmental_data_fields)

        # the the ratios of the subplots. the bottom graph always takes halve of the figure. The top
        # graphs get the rest
        if len(self.plotted_environmental_data_fields) > 0:
            f_sce = 0.5
            f_env = old_div(0.5, len(self.plotted_environmental_data_fields))
            height_ratios = [f_env for i in range(len(self.plotted_environmental_data_fields))] + [
                f_sce]
        else:
            height_ratios = [1]
        self.logger.debug("Setting height ratios {}".format(height_ratios))

        # create the figure and the axis
        self.figure, self.axis = plt.subplots(nrows=self.n_rows, ncols=self.n_cols,
                                              figsize=self.figsize, sharex=True,
                                              gridspec_kw=dict(height_ratios=height_ratios))

        if len(self.plotted_environmental_data_fields) == 0:
            # for only one subplot the subplots returns just the axis, put it in a list
            self.axis = [self.axis]

        # ax_top = list()
        #
        # if self.environmental_combine_plot_lines:
        #     # not working yet. need to check later
        #     ax_top.append(self.axis[0])
        #     ax_top.append(self.axis[0].twinx())
        #
        #     rspine = ax_top[1].spines["right"]
        #     rspine.set_position(("axes", 1.25))
        #     ax_top[1].set_frame_on(False)
        #     ax_top[1].patch.set_visible(False)
        #     self.figure.subplots_adjust(right=0.9)
        # else:
        #     ax_top.append(self.axis[0])

        self.axis[-1].set_xlabel(self.x_label)
        self.figure.canvas.set_window_title(self.window_title)

        self.axis[-1].set_ylabel(self.y_label)

    def run_all(self):
        """
        Here a loop over all the date time is carried out
        """

        self.logger.debug("Start run all")
        if self.progress_bar:
            max_dates = len(self.date_range)
            wdg = PROGRESS_WIDGETS
            date_time = self.date_range[0]
            wdg[-1] = MESSAGE_FORMAT.format(
                progress_bar_message(counter=1, maximum=max_dates, date_time=date_time))
            progress = pb.ProgressBar(widgets=wdg, maxval=max_dates, fd=sys.stdout).start()

        for i_date, start_date_time in enumerate(self.date_range):
            self.logger.info(
                " +++++++++++++ Processing scenarios starting at {} ++++++++++++++ ".format(
                    start_date_time))

            print_banner("Running scenarios starting at {}".format(start_date_time))

            self.scenario.process_scenario(self.environment, start_date_time)

            # end with a report of this scenario
            self.scenario.make_report()

            if self.show_plots or self.save_images:
                self.make_plot(start_date_time)

            if self.progress_bar:
                wdg[-1] = MESSAGE_FORMAT.format(
                    progress_bar_message(i_date, max_dates, start_date_time))
                progress.update(i_date)
                sys.stdout.flush()

        # end of loop with cnt. Close the progress bar
        if self.progress_bar:
            progress.finish()

    def make_plot(self, start_date_time):

        if self.number_of_days_to_plot_before is not None:
            start_date_time_of_plot = start_date_time - pd.Timedelta(
                days=self.number_of_days_to_plot_before)
        else:
            start_date_time_of_plot = start_date_time

        if self.number_of_days_to_plot is not None:
            end_date_time_of_plot = start_date_time + pd.Timedelta(days=self.number_of_days_to_plot)
        else:
            end_date_time_of_plot = self.scenario.end_date_time

        if self.number_of_days_to_plot_after is not None:
            end_date_time_of_plot = end_date_time_of_plot + pd.Timedelta(
                days=self.number_of_days_to_plot_after)

        self.logger.debug(
            "Set start end range {} {}".format(start_date_time, end_date_time_of_plot))

        print_banner("Creating plots for {}".format(start_date_time))

        # loop over all the environmental data quantities
        axis_dict = dict()
        i_quant = 0
        for key, properties in self.environmental_data_fields.items():
            if not properties["show_plot"]:
                continue

            # for later reference create a dictionary to store the properties per axis:
            axis_dict[key] = dict()
            axis_dict[key]["axis"] = self.axis[i_quant]
            axis_dict[key]["labels"] = list()
            axis_dict[key]["lines"] = list()

            ll = self.environmental_data_base.plot(y=[key], ax=self.axis[i_quant], kind="line",
                                                   marker="d",
                                                   markersize=self.marker_size, color="r",
                                                   label=None, legend=None)
            self.axis[i_quant].set_ylabel("{} {}".format(key, properties["units_label"]))
            i_quant += 1

        # add the lines of the current scenario by making use of a class so we can call all
        # scenarios recursively lists plot_lines, line_labels, and y_tick_labels are used to collect
        # all the lines of all scenarios
        plot_lines = list()
        line_labels = list()
        y_tick_labels = dict()
        plot_scenario = ScenarioPlot(scenario=self.scenario, axis=self.axis, axis_dict=axis_dict,
                                     environmental_data_fields=self.environmental_data_fields,
                                     plot_lines=plot_lines, line_labels=line_labels,
                                     marker_size=self.marker_size,
                                     y_tick_labels=y_tick_labels,
                                     legend_x_position=self.legend_x_position,
                                     legend_y_position=self.legend_y_position,
                                     plotted_environmental_data_fields=
                                     self.plotted_environmental_data_fields
                                     )

        self.logger.debug("adding legend {} label = {}".format(plot_lines, line_labels))
        self.axis[-1].legend(plot_lines, line_labels, title="Scopes",
                             bbox_to_anchor=(self.legend_x_position, self.legend_y_position))

        self.axis[-1].set_xlim(start_date_time_of_plot, end_date_time_of_plot)

        self.logger.debug("adding y_tikcs {}".format(y_tick_labels))
        # the y tick labels contain the scenario names which are composed out of its + all the
        # ancestors we can make a selection
        y_tick_new = dict()
        for key, val in y_tick_labels.items():
            family = key.split("_")
            n_ancestors = max(len(family) - 1, 1)
            y_tick_new["\n+".join(family[:n_ancestors])] = val
        y_tick_labels = y_tick_new
        running_values = [y + STATE_DELTA for y in list(y_tick_labels.values())]
        waiting_values = [y - STATE_DELTA for y in list(y_tick_labels.values())]
        self.logger.debug("here running {}".format(running_values))
        self.logger.debug("here waintg {}".format(waiting_values))
        self.axis[-1].set_yticks(list(y_tick_labels.values()) + running_values + waiting_values)
        self.axis[-1].set_yticklabels(
            list(y_tick_labels.keys()) + ["" for i in range(2 * len(running_values))])
        xlim = self.axis[-1].get_xlim()
        x_value = xlim[0] + 0.02 * (xlim[1] - xlim[0])
        for y_value in running_values:
            self.axis[-1].text(x_value, y_value, "Running", horizontalalignment="left",
                               verticalalignment="bottom",
                               fontsize=8, color="blue")
        for y_value in waiting_values:
            self.axis[-1].text(x_value, y_value, "Waiting", horizontalalignment="left",
                               verticalalignment="bottom",
                               fontsize=8, color="blue")
        for y_value in list(y_tick_labels.values()):
            self.axis[-1].text(x_value, y_value, "Idle", horizontalalignment="left",
                               verticalalignment="bottom",
                               fontsize=8, color="blue")

        self.axis[-1].set_ylim(0.5, len(y_tick_labels) + 0.5)

        if self.save_images:
            image_file = "".join(
                [self.results_file_base,
                 "seq_{}".format(start_date_time.strftime("%Y%m%dT%H%M%S"))]) + ".png"
            self.logger.info("Saving plot to {}".format(image_file))
            self.figure.savefig(image_file)


def plot(x, y, data=None, label=None, **kwargs):
    data.plot(x, y, label=label, rot=0, lw=3, grid=False, **kwargs)


class ScenarioPlot(object):
    """
    Class to hold the plot of the scenarios

    Parameters
    ----------
    scenario: :obj:`Scenario`
        Scenario to plot
    axis: Axis
        Reference to the axis of the plot
    axis_dict: dict, optional
    environmental_data_fields: DateFrame, optional
        DateFrame with the environmental data. Default = None
    plot_lines: list, optional
        List of lines to plot. Default = list()
    line_labels: list, optional
        List of labels belonging to the lines. Default = list()
    y_tick_labels: list, optional
        List of y tick labels. Default = list()
    marker_size: int, optional
        Size of the markers. Default = 0 (do not show the markers)
    legend_x_position: float, optional
        X position of the legend. Default = None
    legend_y_position: float, optional
        Y position of the legend. Default = None
    plotted_environmental_data_fields: list, optional
        List of environments already plotted
    """

    def __init__(self, scenario=None, axis=None, axis_dict=None, environmental_data_fields=None,
                 plot_lines=[], line_labels=[], y_tick_labels=dict(),
                 marker_size=0,
                 legend_x_position=None, legend_y_position=None,
                 plotted_environmental_data_fields=[]
                 ):

        self.logger = get_logger(__name__)
        self.logger.info("Adding all the lines of scenario {}".format(scenario.name))

        self.scenario_number = SCENARIO_FAMILY[scenario.name] + 1

        y_tick_labels[scenario.name] = self.scenario_number

        # create a cyclic line style for the thresholds
        line_styles = ["-", "--", "-.", ":"]
        line_style = line_styles[(self.scenario_number - 1) % len(line_styles)]

        # start with the top scenario and keep adding scenarios until there are no more left
        for scope_name, scope in scenario.scopes.items():

            # plot of the state of the current scope
            df = scope.df_data[STATE_FIELD] * STATE_DELTA + self.scenario_number
            ax = df.plot(y=[STATE_FIELD], ax=axis[-1], kind="line", marker="d",
                         markersize=marker_size)
            lines, labels = ax.get_legend_handles_labels()
            line_labels.append(scope_name)
            plot_lines.append(lines[-1])

            # the the color of the line of the scope
            line_color_scope = lines[-1].get_color()

            for key, value in scope.thresholds.items():
                if key not in plotted_environmental_data_fields:
                    continue
                units_label = environmental_data_fields[key]["units_label"]
                if scope.has_changed:
                    self.logger.debug(
                        "Creating data frame column for {} {}".format(key, units_label))
                    # we only need to create the treshold data column in case the scope has changed
                    scope.df_data[key] = np.nan
                    scope.df_data.ix[scope.start_date_time:scope.end_date_time, key] = value
                else:
                    self.logger.debug(
                        "Threshold for {} {} for unchanged. Using the values stored on disk"
                        "".format(key, units_label))

                # make a reference to the axis_dict to put the treshold in the proper axis
                ax = scope.df_data.plot(y=[key], ax=axis_dict[key]["axis"], kind="line",
                                        linestyle=line_style,
                                        color=line_color_scope)
                lines, labels = ax.get_legend_handles_labels()
                axis_dict[key]["labels"].append("{:.1f} {}".format(value, units_label))
                axis_dict[key]["lines"].append(lines[-1])

            if scope.has_changed:
                # the scope data has changed. Write the data again so set next run we can load the
                # threshold as well
                self.logger.debug("Writing thresholds to {}".format(scope.msg_pack_filename))
                scope.df_data.to_msgpack(scope.msg_pack_filename)

            for key, value in scope.thresholds.items():
                if key not in plotted_environmental_data_fields:
                    continue
                axis_dict[key]["axis"].legend(axis_dict[key]["lines"], axis_dict[key]["labels"],
                                              title="Thresholds",
                                              bbox_to_anchor=(legend_x_position, legend_y_position))

            # recursively plot all the other parallel scenarios
            for par_name, parallel_scenario in scope.parallel_scenarios.items():
                self.logger.debug(
                    "Adding the plot lines of the parallel scenario {}".format(par_name))
                par_plot = ScenarioPlot(scenario=parallel_scenario, axis=axis, axis_dict=axis_dict,
                                        environmental_data_fields=environmental_data_fields,
                                        plot_lines=plot_lines, line_labels=line_labels,
                                        y_tick_labels=y_tick_labels,
                                        marker_size=marker_size,
                                        legend_x_position=legend_x_position,
                                        legend_y_position=legend_y_position,
                                        plotted_environmental_data_fields=
                                        plotted_environmental_data_fields
                                        )
