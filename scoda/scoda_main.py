"""
Python version of the scoda sequence tool.

Based on simpy

Author
Eelco van Vliet


"""
from __future__ import division
from __future__ import print_function

import argparse
import logging
import os
import sys
from collections import OrderedDict

import matplotlib.pyplot as plt
import pandas as pd
from hmc_utils.file_and_directory import make_directory
from hmc_utils.misc import (create_logger, clear_argument_list, read_settings_file,
                            print_banner, read_value_from_dict_if_valid, get_clean_version,
                            PackageInfo)
from past.utils import old_div

import _version
from scoda.scoda_classes import ScenarioSequence

import scoda

PACKAGE_INFO = PackageInfo(scoda)
__version__ = PACKAGE_INFO.package_version

logfile = None

if getattr(sys, 'frozen', False):
    # we are running in a bundle
    bundle_dir = sys._MEIPASS
    import versiontag

    __version__ = versiontag.VERSIONTAG
    GIT_SHA = versiontag.GIT_SHA
else:
    # we are running in a normal Python environment
    bundle_dir = os.path.dirname(os.path.abspath(__file__))
    __version__ = _version.get_versions()['version']
    GIT_SHA = _version.get_versions()["full-revisionid"]

# if a file with this name is found in the working directory, stop the batch process and write the
# results
STOP_FILE_NAME = "stop"

sys.stderr.flush()


def parse_the_command_line_arguments(__version__):
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # parse the command line to set some options2
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    parser = argparse.ArgumentParser(description='Sequence tool for waiting on weather analyzes',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    # set the verbosity level command line arguments
    parser.add_argument('-d', '--debug', help="Print lots of debugging statements",
                        action="store_const", dest="log_level", const=logging.DEBUG,
                        default=logging.INFO)
    parser.add_argument('-v', '--verbose', help="Be verbose", action="store_const",
                        dest="log_level",
                        const=logging.INFO)
    parser.add_argument('-q', '--quiet', help="Be quiet: no output", action="store_const",
                        dest="log_level",
                        const=logging.WARNING)
    parser.add_argument("--progress_bar", action="store_true", default=False,
                        help="Just show a progress bar instead of the logging message.)")
    parser.add_argument('--show_plots', help="Show the plot, do not write", action="store_true")
    parser.add_argument('--save_images', help="Save the images", action="store_true")
    parser.add_argument('--reset_data_base', help="Reset the temporary dump of the data base",
                        action="store_true")
    parser.add_argument('--reset_wow_thresholds',
                        help="Reset the Waiting on weather thresholds. This option must be"
                             "given each time one of the threshold any of the scope has been"
                             "changed! ", action="store_true")
    parser.add_argument('configuration_file', help="Settings yaml file")
    parser.add_argument("--version", help="Show the current version", action="store_true")
    parser.add_argument("--version_long", help="Show the full version info including git tag",
                        action="store_true")
    parser.add_argument("--full_revision_id", help="Show the full git sha1 revision id",
                        action="store_true")
    parser.add_argument("--write_log_to_file", action="store_true",
                        help="Write the logging information to file")
    parser.add_argument("--log_file_base", default="log", help="Default name of the logging output")
    parser.add_argument('--log_file_debug', help="Print lots of debugging statements to file",
                        action="store_const", dest="log_level_file", const=logging.DEBUG,
                        default=logging.INFO)
    parser.add_argument('--log_file_verbose', help="Be verbose to file", action="store_const",
                        dest="log_level_file",
                        const=logging.INFO)
    parser.add_argument('--log_file_quiet', help="Be quiet: no output to file",
                        action="store_const",
                        dest="log_level_file", const=logging.WARNING)

    # note that the following option are not parsed via the parser but directly extracted from
    # the sys.argv list. You have to write them out on the command line completely
    if set(sys.argv).intersection(["--version", "--version_long", "--full_revision_id"]):

        # if --version or versionlong is passed on the command line, show the version and exit
        if "--version" in sys.argv:
            # full version is not requested, so clean it
            version = get_clean_version(__version__)
        else:
            version = __version__
        print("version : {}".format(version))
        if "--full_revision_id" in sys.argv:
            print("git SHA        : {}".format(PACKAGE_INFO.git_sha))
            print("Python version : {}".format(PACKAGE_INFO.python_version))
            print("Build date     : {}".format(PACKAGE_INFO.build_date))
        sys.exit(0)

    # parse the command line
    args = parser.parse_args()

    return args, parser


def run():
    sys.argv = clear_argument_list(sys.argv)
    args, parser = parse_the_command_line_arguments(__version__)

    if args.write_log_to_file:
        # http://stackoverflow.com/questions/29087297/
        # is-there-a-way-to-change-the-filemode-for-a-logger-object-that-is-not-configured
        log_file_base = args.log_file_base
        sys.stderr = open(log_file_base + ".err", 'w')
    else:
        log_file_base = None

    logger = create_logger(file_log_level=args.log_level_file, console_log_level=args.log_level,
                           log_file=log_file_base)

    # store the starting time and print a banner to the output
    start_time = pd.to_datetime("now")
    print_banner(
        "{} {} {} @ {}".format(os.path.split(__file__)[1], __version__, args.configuration_file,
                               start_time),
        width=100)

    if args.progress_bar:
        # switch of all logging because we are showing the progress bar via the print statement
        # log.disabled = True
        logger.handlers[0].setLevel(logging.CRITICAL)

    # read the settings file
    try:
        logger.info("Reading configuration file {}".format(args.configuration_file))
        settings = read_settings_file(args.configuration_file)
    except IOError as err:
        parser.error(err)

    logger.debug(settings)

    general_settings = settings["general"]
    plot_settings = settings["plot_settings"]
    environmental_data_settings = settings["environmental_data"]
    output_directory = read_value_from_dict_if_valid(environmental_data_settings,
                                                     "output_directory")
    temporary_directory = read_value_from_dict_if_valid(general_settings, "temporary_directory")
    if temporary_directory is None:
        temporary_directory = os.path.splitext(args.configuration_file)[0] + "_temp"
    if output_directory is None:
        output_directory = os.path.splitext(args.configuration_file)[0] + "_out"
    make_directory(output_directory)
    make_directory(temporary_directory)

    environmental_data_base_file_name = environmental_data_settings["file_name"]
    environmental_temp_out_file = environmental_data_settings["temporary_out_file_name"]
    environmental_data_fields = environmental_data_settings["data_fields"]
    environmental_select_fields = environmental_data_settings[
        "select_required_fields_from_data_base"]

    results_file_base = read_value_from_dict_if_valid(general_settings, "results_file_base",
                                                      "scoda")

    marker_size = read_value_from_dict_if_valid(plot_settings, "marker_size", 0)

    scenario_dict = settings["scenarios"]

    consumable_dict = read_value_from_dict_if_valid(settings, "consumables", OrderedDict())

    number_of_days_to_plot_before = plot_settings["number_of_days_to_plot_before"]
    number_of_days_to_plot_after = plot_settings["number_of_days_to_plot_after"]
    number_of_days_to_plot = plot_settings["number_of_days_to_plot"]
    date_range_dict = settings["date_range"]
    start_date = date_range_dict["start"]
    end_date = read_value_from_dict_if_valid(date_range_dict, "end")
    periods = read_value_from_dict_if_valid(date_range_dict, "periods")
    freq = read_value_from_dict_if_valid(date_range_dict, "freq", "1D")

    date_range = pd.date_range(start=start_date, end=end_date, periods=periods, freq=freq)
    logger.debug("date range: {}".format(date_range))

    sequence = ScenarioSequence(environmental_data_base_file_name=environmental_data_base_file_name,
                                environmental_temp_out_file=environmental_temp_out_file,
                                environmental_data_fields=environmental_data_fields,
                                environmental_select_fields=environmental_select_fields,
                                reset_environmental_data=args.reset_data_base,
                                reset_wow_thresholds=args.reset_wow_thresholds,
                                scenario_dictionary=scenario_dict,
                                consumable_dictionary=consumable_dict,
                                temporary_directory=temporary_directory,
                                output_directory=output_directory,
                                results_file_base=results_file_base,
                                date_range=date_range,
                                number_of_days_to_plot=number_of_days_to_plot,
                                number_of_days_to_plot_before=number_of_days_to_plot_before,
                                number_of_days_to_plot_after=number_of_days_to_plot_after,
                                marker_size=marker_size,
                                save_images=args.save_images,
                                show_plots=args.show_plots,
                                progress_bar=args.progress_bar
                                )

    if args.log_level == logging.DEBUG:
        logger.debug("\n{}".format(sequence.environmental_data_base.info()))

    print_banner("Start the simulation")

    sequence.init_scenario_plot()

    sequence.run_all()

    running_time = pd.to_datetime("now") - start_time
    final_message = "\n\n{}\nComputing time: {} ({:.1f} seconds)\n{}\n" \
                    "".format("*" * 80, running_time, old_div(running_time, pd.Timedelta(1, "s")),
                              "*" * 80)
    if args.progress_bar:
        print(final_message)
    else:
        logger.info(final_message)

    if args.show_plots:
        plt.ioff()
        plt.show()


if __name__ == "__main__":
    run()
