=====
Scoda
=====

This is the documentation of **Scoda**, the Python sequence tool modeller based on Simpy


Contents
========

.. toctree::
   :maxdepth: 2

   readme_link.rst
   License <license>
   Authors <authors>
   Changelog <changes>
   Module Reference <api/modules>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
