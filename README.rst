=====
Scoda
=====


This is a the first Python implementation of the Scoda tool.


Description
===========

The Python Scoda tool is based on simpy_

.. _simpy:
    https://simpy.readthedocs.io/en/latest/



Note
====

This project has been set up using PyScaffold 2.5.6. For details and usage
information on PyScaffold see http://pyscaffold.readthedocs.org/.
